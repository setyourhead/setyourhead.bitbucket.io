
AUTHOR: Ben Connors
UPDATED: 2022-01-26
DESCRIPTION: How we currently encode video for webview

Our current SOP is to encode to Webm vp8 at 720 with no audio as of Jan 26th 2022

Here's the FFMPEG Command:
ffmpeg -i input.mp4 -vcodec libvpx -crf 35 -b:v 1M -an -vf scale=-1:720 output.webm

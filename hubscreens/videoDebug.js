/*
AUTHOR: Ben Connors
UPDATED: 2022-02-04
DESCRIPTION: For Debugging HTML5 Video Players in WebView Screens
*/
var videoElement = document.getElementById("video");
videoElement.onerror = function() {
    console.log("Video: error " + videoElement.error.code + "-" + videoElement.error.message);
}
videoElement.addEventListener('ended', function() {
    console.log('Video: has ended!');
}, false);

videoElement.addEventListener('play', (event) => {
    console.log('Video: is playing.');
});
videoElement.addEventListener('pause', (event) => {
    console.log('Video: is paused.');
});

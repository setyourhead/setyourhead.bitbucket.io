
/*
AUTHOR: Ben Connors
UPDATED: 2022-02-04
DESCRIPTION: works with dashboard.js to built telemetry.html
includes header for table and functions for building telemetry table
leverages telemetry.css for pie charts
TODO: Get Pie Charts working in Gecko Browser (https://www.freecodecamp.org/news/css-only-pie-chart/)
*/

var listHTML = '<table><thead><tr><th></th><th>E</th><th>F</th><th>G</th></tr></thead>';

//TELEM
var possibleLookEvents = 52;
var durInSec = 416;

//Loop Details:
function iterate(entry) {

    var sessionTime = new Date(entry.ts * 1);

    var color2 = [255, 166, 0];
    var color1 = [0, 153, 255];


//Killing pie charts & math for Storm 3/01/2022 version doesn't use Percentages
var aPie= entry.look_event_uniques;
var bPie= Math.round(entry.total_duration);
//Build Pie Chart for "awareness"
/*
    var a = Math.round((entry.look_event_uniques / possibleLookEvents * 100));
    var aColor = pickHex(color1, color2, (a / 100));
    var aPie = `
                 <div class="pie" style="--p:${a};--b:1vw;--c:rgb(${aColor});">${a}</div>
            `
    //Build Pie Chart for "focus"
    var b = Math.round((entry.total_duration / durInSec * 100));
    var bColor = pickHex(color1, color2, (b / 100));
    var bPie = `
                 <div class="pie" style="--p:${b};--b:1vw;--c:rgb(${bColor});">${b}</div>
                 `
*/
    var c = entry.look_count;

    listHTML += `<TR>
                     <TD>${entry.username}</TD>
                     <TD>${aPie}</TD>
                     <TD>${bPie}</TD>
                     <TD>${c}</TD>
                 </TR>`;
}


function pickHex(color1, color2, weight) {
    var p = weight;
    var w = p * 2 - 1;
    var w1 = (w / 1 + 1) / 2;
    var w2 = 1 - w1;
    var rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
        Math.round(color1[1] * w1 + color2[1] * w2),
        Math.round(color1[2] * w1 + color2[2] * w2)
    ];
    return rgb;
}

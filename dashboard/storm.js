/*
AUTHOR: Ben Connors
UPDATED: 2022-12-01
DESCRIPTION: General functions for  telemetry.html & targets.html
Used by targets.js & telemetry.js.
Gets and validates URL parameters.
Builds and calls API QUERY.
*/

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

const cohortId = urlParams.get('cohortId');
// const epoch = urlParams.get('epoch'); - anonymization made this unncessary -BC Nov 30 2022

let count = 0;

/* anonymization made this unncessary -BC Nov 30 2022

        //Catch bad params
        if ((room == undefined) || (epoch == undefined)) {
            console.log(room == undefined);
            console.log(epoch == undefined);
            alert("Your URL is incorect. Please specificy the 'room' and the date as 'epoch'.");
            window.location.replace("index.html");
        } else {

            //Put Date in Header
            window.addEventListener('load', function() {
                //document.getElementById('date').innerHTML = theDate.toDateString();
            })
    */

/* Removed Epoch and localized date building because anonymoization makes it unnecessary -BC Nov 30 2022

        //Turn Date into Epoch for API call
            var theDate = new Date(epoch * 1000);
            var dd = theDate.toLocaleDateString("en-GB", { // you can use undefined as first argument
                day: "2-digit",
            });
            var mm = theDate.toLocaleDateString("en-GB", { // you can use undefined as first argument
                month: "2-digit",
            });
            var yyyy = String(theDate.getFullYear());

            //Start building API CALL
            var cohort_id = yyyy + mm + dd + " " + room;
        */  
    let query = 'cohort_id=' + cohortId + '&sort_by=event_1_start&sort_order=desc'
    var entries;


    // API END POINT
    let apiUrl = "https://sheetdb.io/api/v1/r9a8muj8vuubt/search/?";

    //console.log(apiUrl + query);

    // Get all data sorted by time submitted in ascending order
    axios.get(apiUrl + query)
        .then(response => {

            //Throw it in the console
            console.log(response.data);

            if (response.data.length == 0) {
                alert("There are no matching records in the database at this time.  Please check your date and room. If you believe there is still an error please contact Head Set Studio.");
            }

            //Loop Through It
            response.data.forEach(entry => buildCard(entry));

            //Build List
        });
//} See Line 34


//Loop Details:
function buildCard(entry){

document.getElementById('e1').innerHTML = getIcon(entry.event_1_start);
document.getElementById('e2').innerHTML = getIcon(entry.event_2_start);
document.getElementById('e3').innerHTML = getIcon(entry.event_3_start);
document.getElementById('e4').innerHTML = getIcon(entry.event_4_start);
document.getElementById('count').innerHTML = count ;
document.getElementById('percent').innerHTML = Math.round(entry.look_event_uniques) +'%' ;


}

function getIcon(event_result) {
    if (event_result == 'null') {
        return `
        <span class="fa-stack fa-2x" style="color:#666;">
        <i class="far fa-square fa-stack-2x"></i>
        <i class="fas fa-times fa-stack-1x"></i>
        </span>`;
    } else {
        count++;
        console.log(count);
        return `
        <span class="fa-stack fa-2x">
        <i class="far fa-square fa-stack-2x" style="color:#fff;"></i>
        <i class="fas fa-check fa-stack-1x"></i>
        </span>`;

    }
}

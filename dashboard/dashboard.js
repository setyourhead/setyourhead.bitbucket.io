/*
AUTHOR: Ben Connors
UPDATED: 2022-02-04
DESCRIPTION: General functions for  telemetry.html & targets.html
Used by targets.js & telemetry.js.
Gets and validates URL parameters.
Builds and calls API QUERY.
*/

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

const room = urlParams.get('room');
const epoch = urlParams.get('epoch');


//Catch bad params
if ((room == undefined) || (epoch == undefined)) {
    console.log(room == undefined);
    console.log(epoch == undefined);
    alert("Your URL is incorect. Please specificy the 'room' and the date as 'epoch'.");
    window.location.replace("index.html");
} else {

    //Put Date in Header
    window.addEventListener('load', function() {
        document.getElementById('date').innerHTML = theDate.toDateString();
    })

//Turn Date into Epoch for API call
    var theDate = new Date(epoch * 1000);
    var dd = theDate.toLocaleDateString("en-GB", { // you can use undefined as first argument
        day: "2-digit",
    });
    var mm = theDate.toLocaleDateString("en-GB", { // you can use undefined as first argument
        month: "2-digit",
    });
    var yyyy = String(theDate.getFullYear());

    //Start building API CALL
    var cohort_id = yyyy + mm + dd + " " + room;
    let query = 'cohort_id=' + cohort_id + '&sort_by=event_1_start&sort_order=desc'
    var entries;

    // API END POINT
    let apiUrl = "https://sheetdb.io/api/v1/r9a8muj8vuubt/search/?";
  
    console.log(apiUrl + query);

    // Get all data sorted by time submitted in ascending order
    axios.get(apiUrl + query)
        .then(response => {

            //Throw it in the console
            console.log(response.data);

            if (response.data.length == 0) {
                alert("There are no matching records in the database at this time.  Please check your date and room. If you believe there is still an error please contact Head Set Studio.");
            }

            //Loop Through It
            response.data.forEach(entry => iterate(entry));

            //Build List
            let list = document.querySelector("#sessionList");
            list.innerHTML = listHTML;
        });
}

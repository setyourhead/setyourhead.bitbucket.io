/*
AUTHOR: Ben Connors
UPDATED: 2022-02-04
DESCRIPTION: functions for index.html form
including initialization of datepicker from https://mcdatepicker.netlify.app/
*/

    window.addEventListener('load', function() {
        const picker = MCDatepicker.create({
            el: '#datepicker',
            autoClose: true,
            dateFormat: 'dd mmmm yy'
        });

        button1El = document.getElementById("targetsButton");
        button1El.onclick = function() {
            submit("targets.html")
        };
        button2El = document.getElementById("awarenessButton");
        button2El.onclick = function() {
            submit("telemetry.html")
        };
    });

    function submit(url) {
        let date = document.getElementById("datepicker").value;
        let room = document.getElementById("room").innerHTML;
        let jsdate = new Date(date);
        let time = jsdate.valueOf() / 1000;
        let params = "?room=" + room + "&epoch=" + time;
        window.location.href = url + params;
    }

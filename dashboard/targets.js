/*
AUTHOR: Ben Connors
UPDATED: 2022-02-28
DESCRIPTION: works with dashboard.js to built targets.html
includes header for table and functions for building target table
*/

// Target header
var listHTML = '<table><thead><tr><th></th><th>A</th><th>B</th><th>C</th><th>D</th></tr></thead>';

// Pretty CheckMarks for seen LookEvents, Pretty Xs for unseen ones.
function getIcon(event_result) {
    if (event_result == 'null') {
        return `
        <span class="fa-stack fa-2x" style="color:#666;">
        <i class="far fa-square fa-stack-2x"></i>
        <i class="fas fa-times fa-stack-1x"></i>
        </span>`;
    } else {
        return `
        <span class="fa-stack fa-2x">
        <i class="far fa-square fa-stack-2x" style="color:#fff;"></i>
        <i class="fas fa-check fa-stack-1x"></i>
        </span>`;
    }
}

//Loop Details:
function iterate(entry) {
    var sessionTime = new Date(entry.ts * 1);

    listHTML += `<TR>
                     <TD>${entry.username}</TD>
                     <TD>${getIcon(entry.event_1_start)}</TD>
                     <TD>${getIcon(entry.event_2_start)}</TD>
                     <TD>${getIcon(entry.event_3_start)}</TD>
                     <TD>${getIcon(entry.event_4_start)}</TD>
                 </TR>`;

}
